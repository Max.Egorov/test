# Техническое задание по созданию REST API

### Описание сервиса
Разработка RESTful API, службу поддержки, модель данных для создания банковских счетов, а так же
перевод средств между счетами. Взаимодействие с API будет осуществляться с помощью HTTP-запросов.

#### Список использованных технологий
* Java 17
* Spring Boot
* Spring Data JPA
* Spring Web
* Lombok
* ModelMapper
* PostgreSQL
* H2 Database
* LiquiBase
* Swagger(OpenAPI)

____
### Функциональность

- Управление счетами:
    - cоздание (createAccount),
    - обновление(updateAccount),
    - просмотр(getAccount, getAllAccount),
    - удаление(deleteAccount).
    - Пополнение счета:
    - Пополнение баланса(deposit).
    - Снятие со счета:
    - Уменьшение баланса(withdraw).
    - Перевод со счета:
    - Перевод средств между счетами(transfer).
____

### Документация

Документация сформирована автоматически с помощью Swagger(OpenAPI)
и доступна после запуска проекта по адресу http://localhost:8181/swagger-ui/index.html#/

