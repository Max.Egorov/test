package ru.egorov.practical.test.service.impl;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import ru.egorov.practical.test.model.Account;
import ru.egorov.practical.test.model.Transaction;
import ru.egorov.practical.test.model.dto.*;
import ru.egorov.practical.test.repository.AccountRepository;
import ru.egorov.practical.test.repository.TransactionRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@SpringBootTest
public class AccountServiceImplTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private ModelMapper modelMapper;
    @InjectMocks
    private AccountServiceImpl accountService;

    @Test
    public void testCreateAccount() {

        CreateAccountDto createAccountDto = new CreateAccountDto();
        createAccountDto.setAccountName("TestAccount");

        Account account = new Account();
        account.setAccountName("TestAccount");
        account.setPin("1234");

        InfoAccountDto infoAccountDto = new InfoAccountDto();
        infoAccountDto.setAccountName("TestAccount");

        when(modelMapper.map(eq(createAccountDto), eq(Account.class))).thenReturn(account);
        when(accountRepository.existsByAccountName(eq("TestAccount"))).thenReturn(false);
        when(accountRepository.save(eq(account))).thenReturn(account);
        when(modelMapper.map(eq(account), eq(InfoAccountDto.class))).thenReturn(infoAccountDto);

        InfoAccountDto result = accountService.createAccount(createAccountDto);

        assertNotNull(result);
        assertEquals("TestAccount", result.getAccountName());
    }

    @Test
    public void testUpdateAccount() {

        Long id = 1L;
        UpdatedAccountDto updatedAccountDto = new UpdatedAccountDto();
        updatedAccountDto.setPin("1234");
        updatedAccountDto.setUpdatedAccountName("NewAccountName");

        Account account = new Account();
        account.setId(id);
        account.setAccountName("OldAccountName");
        account.setPin("1234");

        InfoAccountDto infoAccountDto = new InfoAccountDto();
        infoAccountDto.setAccountName("NewAccountName");

        when(accountRepository.findById(eq(id))).thenReturn(Optional.of(account));
        when(accountRepository.existsByAccountName(eq("NewAccountName"))).thenReturn(false);
        when(accountRepository.save(eq(account))).thenReturn(account);
        when(modelMapper.map(eq(account), eq(InfoAccountDto.class))).thenReturn(infoAccountDto);

        InfoAccountDto result = accountService.updateAccount(id, updatedAccountDto);

        assertNotNull(result);
        assertEquals("NewAccountName", result.getAccountName());
    }

    @Test
    public void testGetAccountInfo() {

        Account account = new Account();
        account.setId(1L);
        account.setAccountName("TestAccount");

        AccountDto accountDto = new AccountDto();
        accountDto.setId(1L);
        accountDto.setAccountName("TestAccount");

        when(accountRepository.findById(1L)).thenReturn(java.util.Optional.of(account));
        when(modelMapper.map(account, AccountDto.class)).thenReturn(accountDto);

        AccountDto result = accountService.getAccountInfo(1L);

        assertNotNull(result);
        assertEquals(1L, result.getId());
        assertEquals("TestAccount", result.getAccountName());
    }

    @Test
    public void testGetAllAccount() {

        Account account1 = new Account();
        account1.setId(1L);
        account1.setAccountName("Account1");

        Account account2 = new Account();
        account2.setId(2L);
        account2.setAccountName("Account2");

        List<Account> accountList = Arrays.asList(account1, account2);

        InfoAccountDto infoAccountDto1 = new InfoAccountDto();
        infoAccountDto1.setAccountName("Account1");
        InfoAccountDto infoAccountDto2 = new InfoAccountDto();
        infoAccountDto2.setAccountName("Account2");

        when(accountRepository.findAll()).thenReturn(accountList);
        when(modelMapper.map(eq(account1), eq(InfoAccountDto.class))).thenReturn(infoAccountDto1);
        when(modelMapper.map(eq(account2), eq(InfoAccountDto.class))).thenReturn(infoAccountDto2);

        List<InfoAccountDto> result = accountService.getAllAccount();

        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("Account1", result.get(0).getAccountName());
        assertEquals("Account2", result.get(1).getAccountName());
    }

   @Test
    public void testDeleteAccount() {

        Long id = 1L;

        when(accountRepository.existsById(eq(id))).thenReturn(true);

        accountService.deleteAccount(id);

        verify(accountRepository, times(1)).deleteById(eq(id));
    }

    @Test
    public void testDeposit() {

        Long id = 1L;
        TransactionAccountDto transactionAccountDto = new TransactionAccountDto();
        transactionAccountDto.setTransferAmount(10.0);
        transactionAccountDto.setPin("1234");

        Account account = new Account();
        account.setId(id);
        account.setAccountBalance(10.0);
        account.setPin("1234");

        InfoAccountDto expectedInfoAccountDto = new InfoAccountDto();
        expectedInfoAccountDto.setBalance(20.0);  // Учитывайте, что значение баланса увеличится на 10.0

        when(accountRepository.findById(eq(id))).thenReturn(Optional.of(account));
        when(accountRepository.save(eq(account))).thenReturn(account);
        when(modelMapper.map(eq(account), eq(InfoAccountDto.class))).thenReturn(expectedInfoAccountDto);

        InfoAccountDto result = accountService.deposit(id, transactionAccountDto);

        assertNotNull(result);
        assertEquals(20.0, result.getBalance());

        verify(transactionRepository, times(1)).save(any(Transaction.class));
    }

    @Test
    public void testWithdraw() {

        Long id = 1L;
        TransactionAccountDto transactionAccountDto = new TransactionAccountDto();
        transactionAccountDto.setPin("1234");
        transactionAccountDto.setTransferAmount(50.0);

        Account account = new Account();
        account.setId(id);
        account.setAccountName("TestAccount");
        account.setPin("1234");
        account.setAccountBalance(100.0);

        InfoAccountDto expectedInfoAccountDto = new InfoAccountDto();
        expectedInfoAccountDto.setBalance(50.0);

        when(accountRepository.findById(eq(id))).thenReturn(Optional.of(account));
        when(accountRepository.save(eq(account))).thenReturn(account);
        when(modelMapper.map(eq(account), eq(InfoAccountDto.class))).thenReturn(expectedInfoAccountDto);

        InfoAccountDto result = accountService.withdraw(id, transactionAccountDto);
        assertNotNull(result);
        assertEquals(50.0, result.getBalance(), 0.001); // Проверка баланса с учетом погрешности

        verify(transactionRepository, times(1)).save(any(Transaction.class));
    }

    @Test
    public void testTransfer() {

        Long id = 1L;
        String destinationAccountName = "DestinationAccount";
        TransactionAccountDto transactionAccountDto = new TransactionAccountDto();
        transactionAccountDto.setPin("1234");
        transactionAccountDto.setTransferAmount(50.0);
        transactionAccountDto.setDestinationAccountName(destinationAccountName);

        Account account = new Account();
        account.setId(id);
        account.setAccountName("TestAccount");
        account.setPin("1234");
        account.setAccountBalance(100.0);

        Account destinationAccount = new Account();
        destinationAccount.setAccountName(destinationAccountName);
        destinationAccount.setAccountBalance(50.0);

        InfoAccountDto expectedInfoAccountDto = new InfoAccountDto();
        expectedInfoAccountDto.setBalance(50.0);

        when(accountRepository.findById(eq(id))).thenReturn(Optional.of(account));
        when(accountRepository.findByAccountName(eq(destinationAccountName))).thenReturn(Optional.of(destinationAccount));
        when(accountRepository.save(any(Account.class))).thenAnswer(invocation -> invocation.getArgument(0));
        when(modelMapper.map(any(Account.class), eq(InfoAccountDto.class))).thenReturn(expectedInfoAccountDto);

        InfoAccountDto result = accountService.transfer(id, transactionAccountDto);

        assertNotNull(result);
        assertEquals(50.0, result.getBalance(), 0.001);

        verify(accountRepository, times(1)).findById(eq(id));
        verify(accountRepository, times(1)).findByAccountName(eq(destinationAccountName));
        verify(accountRepository, times(2)).save(any(Account.class));
        verify(transactionRepository, times(2)).save(any(Transaction.class));
    }
}