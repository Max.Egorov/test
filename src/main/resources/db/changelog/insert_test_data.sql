-- Insert data for Test Account 1
INSERT INTO account (id, number, name, balance, pin_code, created_at, updated_at)
VALUES (1, '40817810738261200001', 'Test Account 1', 1000.00, '1234', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

-- Insert data for Test Account 2
INSERT INTO account (id, number, name, balance, pin_code, created_at, updated_at)
VALUES (2, '40817810738261200002', 'Test Account 2', 500.00, '5678', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);

