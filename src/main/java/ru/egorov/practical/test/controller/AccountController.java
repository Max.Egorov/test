package ru.egorov.practical.test.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.egorov.practical.test.controller.api.AccountApi;
import ru.egorov.practical.test.model.dto.*;
import ru.egorov.practical.test.service.AccountService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class AccountController implements AccountApi {

    private final AccountService accountService;

    @Override
    public ResponseEntity<InfoAccountDto> createAccount(@Valid @RequestBody CreateAccountDto createAccountDto) {
        InfoAccountDto createdAccountInfo = accountService.createAccount(createAccountDto);
        return ResponseEntity.ok(createdAccountInfo);
    }

    @Override
    public ResponseEntity<InfoAccountDto> updateAccount(
            @PathVariable Long id,
            @Valid @RequestBody UpdatedAccountDto updatedAccountDto
    ) {
        InfoAccountDto updatedInfoAccount = accountService.updateAccount(id, updatedAccountDto);
        return ResponseEntity.ok(updatedInfoAccount);
    }

    @Override
    public ResponseEntity<AccountDto> getAccount(@PathVariable Long id) {
        AccountDto accountDto = accountService.getAccountInfo(id);
        return ResponseEntity.ok(accountDto);
    }

    @Override
    public ResponseEntity<List<InfoAccountDto>> getAllAccount(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "pageSize", required = false) Integer pageSize
    ) {
        if (page == null || pageSize == null) {
            return ResponseEntity.ok(accountService.getAllAccount());
        }

        PageRequest pageRequest = PageRequest.of(page, pageSize);
        return ResponseEntity.ok(accountService.getAllAccount(pageRequest));
    }

    @Override
    public ResponseEntity<Void> deleteAccount(@PathVariable Long id) {
        accountService.deleteAccount(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<InfoAccountDto> deposit(
            @PathVariable Long id,
            @Valid @RequestBody TransactionAccountDto transactionAccountDto
    ) {
        InfoAccountDto updatedInfoAccount = accountService.deposit(id, transactionAccountDto);
        return ResponseEntity.ok(updatedInfoAccount);
    }

    @Override
    public ResponseEntity<InfoAccountDto> withdraw(
            @PathVariable Long id,
            @Valid @RequestBody TransactionAccountDto accountTransactionDTO
    ) {
        InfoAccountDto updatedInfoAccount = accountService.withdraw(id, accountTransactionDTO);
        return ResponseEntity.ok(updatedInfoAccount);
    }

    @Override
    public ResponseEntity<InfoAccountDto> transfer(
            @PathVariable Long id,
            @Valid @RequestBody TransactionAccountDto transactionAccountDto
    ) {
        InfoAccountDto updatedInfoAccount = accountService.transfer(id, transactionAccountDto);
        return ResponseEntity.ok(updatedInfoAccount);
    }
}
