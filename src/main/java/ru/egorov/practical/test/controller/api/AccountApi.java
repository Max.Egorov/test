package ru.egorov.practical.test.controller.api;

import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.egorov.practical.test.model.dto.*;

import java.util.List;

@RequestMapping("/api/account")
public interface AccountApi {

    @PostMapping("/")
    ResponseEntity<InfoAccountDto> createAccount(@Valid @RequestBody CreateAccountDto createAccountDto);

    @PutMapping("/{id}")
    ResponseEntity<InfoAccountDto> updateAccount(@PathVariable Long id, @Valid @RequestBody UpdatedAccountDto updatedAccountDto);

    @GetMapping("/{id}")
    ResponseEntity<AccountDto> getAccount(@PathVariable Long id);

    @GetMapping()
    ResponseEntity<List<InfoAccountDto>> getAllAccount(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "pageSize", required = false) Integer pageSize
    );

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteAccount(@PathVariable Long id);

    @PostMapping("/{id}/deposit")
    ResponseEntity<InfoAccountDto> deposit(
            @PathVariable Long id,
            @Valid @RequestBody TransactionAccountDto transactionAccountDto
    );

    @PostMapping("/{id}/withdraw")
    ResponseEntity<InfoAccountDto> withdraw(
            @PathVariable Long id,
            @Valid @RequestBody TransactionAccountDto accountTransactionDTO
    );

    @PostMapping("/{id}/transfer")
    ResponseEntity<InfoAccountDto> transfer(
            @PathVariable Long id,
            @Valid @RequestBody TransactionAccountDto transactionAccountDto
    );
}
