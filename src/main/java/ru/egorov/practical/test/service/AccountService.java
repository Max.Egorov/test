package ru.egorov.practical.test.service;

import org.springframework.data.domain.PageRequest;
import ru.egorov.practical.test.model.dto.AccountDto;
import ru.egorov.practical.test.model.dto.CreateAccountDto;
import ru.egorov.practical.test.model.dto.InfoAccountDto;
import ru.egorov.practical.test.model.dto.TransactionAccountDto;
import ru.egorov.practical.test.model.dto.UpdatedAccountDto;

import java.util.List;

public interface AccountService {

    InfoAccountDto createAccount(CreateAccountDto createAccountDto);
    InfoAccountDto updateAccount(Long id, UpdatedAccountDto updatedAccountDto);
    AccountDto getAccountInfo(Long id);
    List<InfoAccountDto> getAllAccount();
    List<InfoAccountDto> getAllAccount(PageRequest pageRequest);
    void deleteAccount(Long id);
    InfoAccountDto deposit(Long id, TransactionAccountDto transactionAccountDto);
    InfoAccountDto withdraw(Long id, TransactionAccountDto transactionAccountDto);
    InfoAccountDto transfer(Long id, TransactionAccountDto transactionAccountDto);

}
