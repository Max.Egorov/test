package ru.egorov.practical.test.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.egorov.practical.test.exeption.*;
import ru.egorov.practical.test.model.Account;
import ru.egorov.practical.test.model.Transaction;
import ru.egorov.practical.test.model.TransactionType;
import ru.egorov.practical.test.model.dto.*;
import ru.egorov.practical.test.repository.AccountRepository;
import ru.egorov.practical.test.repository.TransactionRepository;
import ru.egorov.practical.test.service.AccountService;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;
    private final ModelMapper modelMapper;
    @Override
    public InfoAccountDto createAccount(CreateAccountDto createAccountDto) {

        String accountName = createAccountDto.getAccountName();

        try {
            if (StringUtils.isEmpty(accountName)) {
                log.error("новая запись о банковском счете не сохранена");
                throw new IllegalArgumentException("Ошибка при создании банковского счета. Название счета не может быть пустым");
            }

            if (existsByAccountName(accountName)) {
                log.error("новая запись о банковском счете: {} не сохранена", accountName);
                throw new AccountWithNameAlreadyExistsException("Ошибка при создании банковского счета. Название счета: " + accountName + " уже используется");
            }
        } catch (IllegalArgumentException | AccountWithNameAlreadyExistsException ex) {
            throw new AccountCreatedException(ex.getMessage());
        }

        Account account = modelMapper.map(createAccountDto, Account.class);
        account.setPin(account.getPin());
        account = accountRepository.save(account);

        log.info("новая запись о банковском счете: {} успешно сохранена", accountName);
        return modelMapper.map(account, InfoAccountDto.class);
        }

    @Override
    public InfoAccountDto updateAccount(Long id, UpdatedAccountDto updatedAccountDto)
            throws AccountAccessException, AccountNotFoundException, AccountWithNameAlreadyExistsException {

        Account account = getAccount(id);

        if (!updatedAccountDto.getPin().equals(account.getPin())) {
            log.error("название счета: {} не обновлено", account.getAccountName());
            throw new AccountAccessException("Введен неверный пин код");
        }

        String newAccountName = updatedAccountDto.getUpdatedAccountName();

        try {
            if (StringUtils.isEmpty(newAccountName)) {
                log.error("название счета не обновлено");
                throw new IllegalArgumentException("Название счета не должно быть пустым");
            }

            if (accountRepository.existsByAccountName(newAccountName)) {
                log.error("название счета: {} не обновлено", account.getAccountName());
                throw new AccountWithNameAlreadyExistsException("Ошибка при обновлении банковского счета. Название счета: " + newAccountName + " уже используется");
            }
        } catch (IllegalArgumentException | AccountWithNameAlreadyExistsException ex) {
            throw new AccountCreatedException(ex.getMessage());
        }

        account.setAccountName(newAccountName);
        account = accountRepository.save(account);

        log.info("название счета: {} успешно обновлено", account.getAccountName());
        return modelMapper.map(account, InfoAccountDto.class);
    }


    @Override
    public AccountDto getAccountInfo(Long id) {
        Account account = getAccount(id);
        return modelMapper.map(account, AccountDto.class);
    }

    @Override
    public List<InfoAccountDto> getAllAccount() {
        return accountRepository.findAll().stream()
                .map(account -> modelMapper.map(account, InfoAccountDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<InfoAccountDto> getAllAccount(PageRequest pageRequest) {
        return accountRepository.findAll(pageRequest).stream()
                .map(account -> modelMapper.map(account, InfoAccountDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteAccount(Long id) {
        if (notExistAccountById(id)) {
            log.error("счет с идентификатором: {} не удален", id);
            throw new AccountNotFoundException("Счет с идентификатором: " + id + " не найден");
        }
        accountRepository.deleteById(id);

        log.info("счет с идентификатором: {} успешно удален", id);
    }

    @Override
    public InfoAccountDto deposit(Long id, TransactionAccountDto transactionAccountDto) {
        Account account = getAccount(id);
        Double amount = transactionAccountDto.getTransferAmount();

        if (amount == null || amount <= 0) {
            log.error("счет с названием: {} не пополнен", account.getAccountName());
            throw new AccountWithdrawException("Сумма пополнения должна быть положительной и не может быть пустой");
        }

        account.setAccountBalance(account.getAccountBalance() + amount);
        account = accountRepository.save(account);

        saveTransaction(account, amount, TransactionType.DEPOSIT);

        log.info("счет с названием: {} успешно пополнен на сумму: {}", account.getAccountName(), amount);
        return modelMapper.map(account, InfoAccountDto.class);
    }

    @Override
    public InfoAccountDto withdraw(Long id, TransactionAccountDto transactionAccountDto) {
        Account account = getAccount(id);

        if (!transactionAccountDto.getPin().equals(account.getPin())) {
            log.error("списание средств со счета с названием: {} не прошло", account.getAccountName());
            throw new AccountAccessException("Введен неверный пин код");
        }

        Double amount = transactionAccountDto.getTransferAmount();

        try {
            if (amount == null || amount <= 0) {
                throw new AccountWithdrawException("Сумма списания должна быть положительной и не может быть пустой");
            }

            if (account.getAccountBalance() - amount < 0) {
                throw new AccountWithdrawException("Сумма списания должна быть не больше текущего баланса");
            }
        } catch (AccountWithdrawException awEx) {
            log.error("списание средств со счета с названием: {} не прошло", account.getAccountName());
            throw new AccountWithdrawException(awEx.getMessage());
        }

        saveTransaction(account, amount, TransactionType.WITHDRAW);

        log.info("списание средств на сумму: {} со счета с названием: {} успешно прошло", amount, account.getAccountName());
        return modelMapper.map(account, InfoAccountDto.class);
    }

    @Override
    public InfoAccountDto transfer(Long id, TransactionAccountDto transactionAccountDto) {
        Account account = getAccount(id);

        if (!transactionAccountDto.getPin().equals(account.getPin())) {
            log.error("перевод средств со счета с названием: {} не прошел", account.getAccountName());
            throw new AccountAccessException("Введен неверный пин код");
        }

        String sourceAccountName = transactionAccountDto.getDestinationAccountName();
        Account sourceAccount = getAccountByName(sourceAccountName);

        Double amount = transactionAccountDto.getTransferAmount();

        try {
            if (amount == null || amount <= 0) {
                throw new AccountWithdrawException("Сумма перевода должна быть положительной и не может быть пустой");
            }

            if (account.getAccountBalance() - amount < 0) {
                throw new AccountWithdrawException("Сумма перевода должна быть не больше текущего баланса");
            }
        } catch (AccountWithdrawException awEx) {
            log.error("перевод средств со счета с названием: {} не прошел", account.getAccountName());
            throw new AccountWithdrawException(awEx.getMessage());
        }

        account.setAccountBalance(account.getAccountBalance() - amount);
        account = accountRepository.save(account);

        saveTransaction(account, amount, TransactionType.TRANSFER);

        sourceAccount.setAccountBalance(sourceAccount.getAccountBalance() + amount);
        accountRepository.save(sourceAccount);

        saveTransaction(sourceAccount, amount, TransactionType.TRANSFER);

        log.info("перевод средств на сумму: {} со счета с названием: {} успешно прошел", amount, account.getAccountName());
        return modelMapper.map(account, InfoAccountDto.class);
    }

    private Account getAccount(Long id)
            throws AccountNotFoundException {
        Optional<Account> account = accountRepository.findById(id);
        if (account.isEmpty()) {
            log.error("счет с идентификатором: {} не найден", id);
            throw new AccountNotFoundException("cчет с идентификатором: " + id + " не найден");
        }
        log.info("счет с идентификатором: {} успешно найден", id);
        return account.get();
    }

    private Account getAccountByName(String accountName)
            throws AccountNotFoundException {
        Optional<Account> account = accountRepository.findByAccountName(accountName);

        if (account.isEmpty()) {
            log.error("счет с названием: {} не найден", accountName);
            throw new AccountNotFoundException("Счет с названием: " + accountName + " не найден");
        }

        log.info("счет с названием: {} успешно найден", accountName);
        return account.get();
    }

    private boolean notExistAccountById(Long id) {
        return !accountRepository.existsById(id);
    }

    private boolean existsByAccountName(String accountName) {
        return accountRepository.existsByAccountName(accountName);
    }

    private void saveTransaction(Account account, Double amount, TransactionType transactionType) {
        Transaction transaction = new Transaction();
        transaction.setAccount(account);
        transaction.setAmount(amount);
        transaction.setTransactionType(transactionType);
        transaction.setTransactionTime(LocalDateTime.now());

        transactionRepository.save(transaction);
    }
}
