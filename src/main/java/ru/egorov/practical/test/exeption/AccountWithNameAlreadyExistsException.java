package ru.egorov.practical.test.exeption;

public class AccountWithNameAlreadyExistsException extends RuntimeException{
    public AccountWithNameAlreadyExistsException(String message) {
        super(message);
    }
}
