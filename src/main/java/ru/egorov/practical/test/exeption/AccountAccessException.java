package ru.egorov.practical.test.exeption;

public class AccountAccessException extends RuntimeException{
    public AccountAccessException(String message) {
        super(message);
    }
}
