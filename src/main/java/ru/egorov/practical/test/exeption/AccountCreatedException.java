package ru.egorov.practical.test.exeption;

public class AccountCreatedException extends RuntimeException{
    public AccountCreatedException(String message) {
        super(message);
    }
}
