package ru.egorov.practical.test.exeption;

public class AccountWithdrawException extends RuntimeException{
    public AccountWithdrawException(String message) {
        super(message);
    }
}
