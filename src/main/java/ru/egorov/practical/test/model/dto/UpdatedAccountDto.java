package ru.egorov.practical.test.model.dto;

import lombok.Data;

@Data
public class UpdatedAccountDto {
    private String updatedAccountName;
    private String pin;
}
