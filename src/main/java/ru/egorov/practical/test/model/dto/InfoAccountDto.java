package ru.egorov.practical.test.model.dto;

import lombok.Data;

@Data
public class InfoAccountDto {
    private String accountName;
    private Double balance;
}
