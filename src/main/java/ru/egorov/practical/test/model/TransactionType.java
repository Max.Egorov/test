package ru.egorov.practical.test.model;

public enum TransactionType {
    DEPOSIT,
    WITHDRAW,
    TRANSFER
}
