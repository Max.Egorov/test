package ru.egorov.practical.test.model.dto;

import lombok.Data;

@Data
public class CreateAccountDto {
    private String accountName;
    private Double balance;
}
