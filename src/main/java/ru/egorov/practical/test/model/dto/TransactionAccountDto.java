package ru.egorov.practical.test.model.dto;

import lombok.Data;

@Data
public class TransactionAccountDto {
    private String destinationAccountName;
    private Double transferAmount;
    private String pin;
}
