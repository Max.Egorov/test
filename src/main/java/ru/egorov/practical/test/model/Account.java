package ru.egorov.practical.test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;

@Entity
@Table(name = "account")
@Data
@NoArgsConstructor
public class Account {

    private static final String CODE_BANK = "4081781073826120000";

    private static int number = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number")
    private String accountNumber = CODE_BANK + String.format("%06d", number++);

    @Column(name = "name")
    private String accountName;

    @Column(name = "balance")
    private Double accountBalance = 0.;

    @Column(name = "pin_code")
    private String pin;

    @Column(name = "created_at")
    @CreationTimestamp
    @JsonIgnore
    private Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    @JsonIgnore
    private Date updatedAt;
}

