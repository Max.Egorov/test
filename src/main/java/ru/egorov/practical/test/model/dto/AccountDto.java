package ru.egorov.practical.test.model.dto;

import lombok.Data;

@Data
public class AccountDto {
    private Long id;
    private String accountName;
    private Double balance;
}
